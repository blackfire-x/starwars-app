import { Component, OnInit } from '@angular/core';
import { StarWarsServiceService } from './star-wars-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Star-wars-app';
  page = 1;
  pageSize = 4;
  collectionSize ;
  users:any; 
constructor(private poeople:StarWarsServiceService,private modalService: NgbModal,private route:Router) { 
  

} 
  ngOnInit(): void {
    
      this.poeople.getallpeople().subscribe(users=>{
        console.log(users)
      this.users=users;
      this.collectionSize=this.users.results.length;
      })

  }

  get countries() {
        return this.users.results
          .map((countries, i) => ({id: i + 1, ...countries}))
          .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
      }



}
