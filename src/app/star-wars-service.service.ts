import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class StarWarsServiceService {
  public host:String="https://swapi.dev/api" ;

  constructor(private http:HttpClient) { 

  }
  public getallpeople()
  {
    return this.http.get(this.host+"/people")
  }
}
